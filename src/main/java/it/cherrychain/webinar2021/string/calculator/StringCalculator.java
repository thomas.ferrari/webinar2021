package it.cherrychain.webinar2021.string.calculator;

import static java.lang.Integer.parseInt;

public class StringCalculator {

    public static final String DELIMITERS = ",|\n";

    public int add(String numbers) {
        if (isNumbersEmpty(numbers))
            return 0;

        return sumOf(numbers);
    }

    private int sumOf(String numbers) {
        String[] values = numbers.split(DELIMITERS);
        int result = 0;
        for (String value : values) {
            result += parseInt(value);
        }
        return result;
    }

    private boolean isNumbersEmpty(String numbers) {
        return numbers.length() == 0;
    }

}
