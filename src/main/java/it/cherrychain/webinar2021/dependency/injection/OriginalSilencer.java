package it.cherrychain.webinar2021.dependency.injection;

public class OriginalSilencer implements Silencer {
    public Integer additionalSpeed() {
        return -15;
    }
}
