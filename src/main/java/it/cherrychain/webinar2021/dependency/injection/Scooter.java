package it.cherrychain.webinar2021.dependency.injection;

public class Scooter {

    private Silencer silencer;
    private Engine engine;

    public Scooter(Silencer silencer, Engine engine) {
        this.silencer = silencer;
        this.engine = engine;
    }

    private Integer maxSpeed() {
        return engine.declaredSpeed() + silencer.additionalSpeed();
    }

    public static void main(String[] args) {
        Scooter scooter = new Scooter(new OriginalSilencer(), new Engine50());

        System.out.println("Velocità max raggiunta: " + scooter.maxSpeed());
    }

}
