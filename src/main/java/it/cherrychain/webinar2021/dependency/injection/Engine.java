package it.cherrychain.webinar2021.dependency.injection;

public interface Engine {
    Integer declaredSpeed();
}
