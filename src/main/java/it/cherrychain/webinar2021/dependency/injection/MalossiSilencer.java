package it.cherrychain.webinar2021.dependency.injection;

public class MalossiSilencer implements Silencer {
    @Override
    public Integer additionalSpeed() {
        return 15;
    }
}
