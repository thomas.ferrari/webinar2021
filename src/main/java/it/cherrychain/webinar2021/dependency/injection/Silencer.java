package it.cherrychain.webinar2021.dependency.injection;

public interface Silencer {
    Integer additionalSpeed();
}
