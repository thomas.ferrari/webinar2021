package it.cherrychain.webinar2021.unit.test.injection;

import java.util.ArrayList;
import java.util.List;

public class InMemoryReceipts implements Receipts {

    private final List<Receipt> receipts = new ArrayList<>();

    public void add(Receipt receipt) {
        receipts.add(receipt);
    }

    public List<Receipt> getAll() {
        return receipts;
    }
}
