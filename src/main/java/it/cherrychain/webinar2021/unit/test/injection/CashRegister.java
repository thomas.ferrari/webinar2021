package it.cherrychain.webinar2021.unit.test.injection;

public class CashRegister {

    private final Receipts receipts;

    public CashRegister(Receipts receipts) {
        this.receipts = receipts;
    }

    public void sell(Integer quantity, Double price) {
        receipts.add(new Receipt(quantity, price));
    }

    public Double maxPrice() {
        return receipts.getAll().stream()
                .map(Receipt::price)
                .mapToDouble(a -> a)
                .max()
                .orElse(0.0);
    }

    public Double maxAmount() {
        return receipts.getAll().stream()
                .map(receipt -> receipt.price() * receipt.quantity())
                .mapToDouble(a -> a)
                .max()
                .orElse(0.0);
    }
}
