package it.cherrychain.webinar2021.unit.test;

import java.util.Arrays;
import java.util.List;

public class Calculator {

    public Integer sum(Integer value1, Integer value2) throws NegativeNumberExcepiton {
        return sum(Arrays.asList(value1, value2));
    }

    public Integer sum(List<Integer> values) throws NegativeNumberExcepiton {
        if (values.stream().anyMatch(value -> value < 0)) {
            throw new NegativeNumberExcepiton();
        }

        Integer result = 0;
        for (Integer value :values)  {
            result += value; 
        }
        
        return result;
    }

    public class NegativeNumberExcepiton extends Throwable {
    }
}
