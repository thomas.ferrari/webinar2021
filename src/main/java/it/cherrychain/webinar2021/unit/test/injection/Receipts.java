package it.cherrychain.webinar2021.unit.test.injection;

import java.util.List;

public interface Receipts {
    void add(Receipt receipt);

    List<Receipt> getAll();
}
