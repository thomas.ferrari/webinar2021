package it.cherrychain.webinar2021.unit.test.injection;

import java.util.Objects;

public class Receipt {
    private final Integer quantity;
    private final Double price;

    public Receipt(Integer quantity, Double price) {
        this.quantity = quantity;
        this.price = price;
    }

    public double price() {
        return price;
    }

    public double quantity() { return quantity; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Receipt product = (Receipt) o;
        return Objects.equals(quantity, product.quantity) &&
                Objects.equals(price, product.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(quantity, price);
    }

    @Override
    public String toString() {
        return "Product{" +
                "quantity=" + quantity +
                ", price=" + price +
                '}';
    }
}
