package it.cherrychain.webinar2021.elephant.carpaccio;

import java.util.Map;

public class DefaultTax implements Tax {
    private static Map<String, Double> taxRating = Map.of(
            "TX", 0.0625,
            "NV", 0.08,
            "CA", 0.0825
    );

    @Override
    public Double calculate(double amount, String country) {
        return amount * taxRating.get(country);
    }

}
