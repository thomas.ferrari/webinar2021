/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.cherrychain.webinar2021.elephant.carpaccio;

/**
 *
 * @author utente
 */
public interface Discount {
    Double calculate(double amount);
}
