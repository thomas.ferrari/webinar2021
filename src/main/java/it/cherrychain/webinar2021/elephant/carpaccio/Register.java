package it.cherrychain.webinar2021.elephant.carpaccio;

import java.util.Map;

public class Register {

    private final Tax tax;
    private final Discount discount;

    public Register(Tax tax, Discount discount) {
        this.tax = tax;
        this.discount = discount;
    }

    public Double totalAmount(int quantity, double price, String country) {
        double amount = price * quantity;
        double amountDiscounted = amount - discount.calculate(amount);
        return amountDiscounted + tax.calculate(amountDiscounted, country);
    }

}
