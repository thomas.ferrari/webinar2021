package it.cherrychain.webinar2021.elephant.carpaccio;

public interface Tax {
    Double calculate(double anyDouble, String anyString);
}
