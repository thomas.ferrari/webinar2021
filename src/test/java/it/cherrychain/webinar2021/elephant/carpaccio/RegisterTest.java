package it.cherrychain.webinar2021.elephant.carpaccio;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyDouble;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class RegisterTest {

    @Mock
    private Tax tax;
    @Mock
    private Discount discount;
    private Register register;

    @BeforeEach
    void setUp() {
        register = new Register(tax, discount);

    }

    @Test
    void should_get_total_amount() {
        when(tax.calculate(100, "XXX")).thenReturn(1.0);

        Double totalAmount = register.totalAmount(10, 10, "XXX");

        assertEquals(101, totalAmount);
    }
    
    @Test
    void should_get_discount_total_amount() {
        when(discount.calculate(anyDouble())).thenReturn(1.0);
        
        Double totalAmount = register.totalAmount(10, 10, "XXX");
        
        verify(discount).calculate(anyDouble());
        assertEquals(99, totalAmount);
    }
}
