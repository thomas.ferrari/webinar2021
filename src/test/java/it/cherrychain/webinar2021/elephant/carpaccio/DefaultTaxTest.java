package it.cherrychain.webinar2021.elephant.carpaccio;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DefaultTaxTest {

    @Test
    void should_calculate_tax() {
        Double tax = new DefaultTax().calculate(100, "TX");

        assertEquals(6.25, tax);
    }
}
