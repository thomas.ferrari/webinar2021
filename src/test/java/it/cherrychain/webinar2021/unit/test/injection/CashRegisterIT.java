package it.cherrychain.webinar2021.unit.test.injection;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CashRegisterIT {

    @Test
    void QUESTO_E_UN_TEST_DI_INTEGRAZIONE() {
        CashRegister register = new CashRegister(new InMemoryReceipts());

        register.sell(1, 1.0);

        assertEquals(1.0, register.maxPrice());
        assertEquals(1, register.maxAmount());
    }
}
