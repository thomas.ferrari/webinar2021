package it.cherrychain.webinar2021.unit.test.injection;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CashRegisterTest {

    @Mock
    private Receipts receipts;

    private CashRegister register;

    @BeforeEach
    void setUp() {
        register = new CashRegister(receipts);
    }

    @Test
    void should_sell_a_product() {
        register.sell(1, 2.0);

        verify(receipts).add(new Receipt(1, 2.0));
    }

    @Test
    void should_return_0_as_max_price_when_not_exist_recipt() {
        when(receipts.getAll()).thenReturn(Arrays.asList());

        Double maxPrice = register.maxPrice();

        assertEquals(0.0, maxPrice);
    }

    //TODO un test per testare maxPrice quando getAll mi ritorna più elementi
    //TODO un test per testare maxAmount quando getAll non ritorna elementi
    //TODO un test per testare maxAmount quando getAll mi ritorna più elementi
}
