package it.cherrychain.webinar2021.unit.test;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

    private Calculator calculator;

    @BeforeEach
    void setUp() {
        calculator = new Calculator();
    }

    @Test
    void should_sum_two_values() throws Calculator.NegativeNumberExcepiton {
        assertEquals(2, calculator.sum(1, 1));//assert
    }

    @Test
    void should_get_negative_number_exception_if_sum_negative_values() {
        assertThrows(Calculator.NegativeNumberExcepiton.class, () -> {
            calculator.sum(-1000, 0);
        });
    }

    @Test
    void should_sum_many_values() throws Calculator.NegativeNumberExcepiton {
        assertEquals(2, calculator.sum(Arrays.asList(1, 1)));
    }

    @Test
    void should_return_zero_if_values_is_empty() throws Calculator.NegativeNumberExcepiton {
        Integer result = calculator.sum(Arrays.asList());

        assertEquals(0, result);
    }

    @Test
    void should_get_negative_number_exception_when_sum_negative_values_in_sum_of_many_values() {
        assertThrows(Calculator.NegativeNumberExcepiton.class, () -> {
            calculator.sum(Arrays.asList(1, -20000));
        });
    }
}
