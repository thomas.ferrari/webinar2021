package it.cherrychain.webinar2021.string.calculator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StringCalculatorTest {

    private StringCalculator calculator;

    @BeforeEach
    void setUp() {
        calculator = new StringCalculator();
    }

    @Test
    void should_return_0_with_empty_input() {
        int result = calculator.add("");

        assertEquals(0, result);
    }

    @Test
    void should_return_the_sum_of_many_values() {
        int result = calculator.add("1,2,3");

        assertEquals(6, result);
    }

    @Test
    void should_evaluate_new_line_as_delimiter() {
        int result = calculator.add("1\n2,3");

        assertEquals(6, result);
    }
}
